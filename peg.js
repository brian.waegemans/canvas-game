export class Peg {
    x;
    y;
    radius;
    status;
    bonus;
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.radius = 5;
        this.status = 0;
        this.bonus = Math.random() < 0.1;
    }
    // check if a peg overlaps with a already created one
    pegOverlap(pegs){
        for (const peg of pegs) {
            // is peg within square surrounding old peg?
            if (Math.abs(peg.x - this.x) < 11 && Math.abs(peg.y - this.y) < 11){
                // calculate exact cartesian distance
                if (Math.sqrt(Math.pow(this.x - peg.x, 2) + Math.pow(this.y - peg.y, 2)) < 11) {
                    return true
                }
            }

        }
        return false;
    }
}