import {Ball} from "./ball.js";
import {Peg} from "./peg.js";
import {Basket} from "./basket.js";

// Canvas variables
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 700;
const WIDTH = canvas.width;
const HEIGHT = canvas.height;
canvas.style.border = "1px solid black";

// Game variables
const speed = 100;
const framerate = 60;
const pegs = [];
const balls = [new Ball(WIDTH/2, 12)];
const basket = new Basket(100, 8, HEIGHT);
let lives = 5;
let again = false;
let score = 0;
let ballPoints = 0;
let multiplierBall = 1;
let addNewBall = false;
let temporaryControl = false;
let moveRight = false;
let moveLeft = false;

// create a random peg inside bounds
function randomizePeg(){
    let xMin = 6;
    let xMax = canvas.width - xMin;
    let xRandom = Math.floor(Math.random()* (xMax - xMin + 1)) + xMin;

    let yMin = 150;
    let yMax = canvas.height - 70;
    let yRandom = Math.floor(Math.random()* (yMax - yMin + 1)) + yMin;

    return new Peg(xRandom,yRandom);
}
// create up to 100 random pegs
function initializePegs(){
    for(let i = 0; i < 70; i++) {
        let randomPeg = randomizePeg();
        if(!randomPeg.pegOverlap(pegs)) {
            pegs.push(randomPeg);
        }
    }
}
// Add draw functions to classes, since cxt dependent
Peg.prototype.drawPeg = function () {
    ctx.fillStyle="rgb(0,200,50)";
    ctx.strokeStyle = "rgb(150,150,150)";
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.stroke();
}
Peg.prototype.drawPegBonus = function () {
    ctx.fillStyle="rgb(253,200,1)";
    ctx.strokeStyle = "rgb(150,150,150)";
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.stroke();
}
Ball.prototype.drawBall =  function () {
    ctx.fillStyle="rgb(200,0,0)";
    ctx.strokeStyle = "rgb(0,0,0)";
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.stroke();
}
Basket.prototype.drawBasket = function () {
    ctx.fillStyle = "rgb(0,0,200)";
    ctx.fillRect(basket.x, HEIGHT - basket.height, basket.width, basket.height);
}
// draw functions for game dependent information
function drawLives() {
    ctx.font = "20px Arial";
    ctx.fillStyle = "rgb(0,0,0)";
    ctx.fillText(`Lives: ${lives}`, 10, 24);
}

function drawScore() {
    ctx.font = "20px Arial";
    ctx.fillStyle = "rgb(0,0,0)";
    ctx.fillText(`Points: ${score}`, WIDTH * 2 / 3, 24);
}

function drawAddPoints() {
    ctx.font = "20px Arial";
    ctx.fillStyle = "rgb(0,0,0)";
    ctx.fillText(`+ ${ballPoints} Combo: x${multiplierBall}`, 2* WIDTH/3, 50);
}

function drawLine() {
    // create dummy ball and trace its path
    let dummyBall = new Ball(WIDTH/2, 12);
    ctx.moveTo(dummyBall.x, dummyBall.y);
    dummyBall.vector[0] = mouseX - dummyBall.x;
    dummyBall.vector[1] = mouseY - dummyBall.y;
    dummyBall.normalizeVec();
    for (let i = 0; i < 200; i++) {
        dummyBall.checkCollisionSides(WIDTH, HEIGHT);
        if (dummyBall.checkCollisionPegs(pegs)) {
            // after peg contact, only trace line for one frame
            i = 199;
        }
        dummyBall.move(speed, framerate);
        ctx.lineTo(dummyBall.x, dummyBall.y);
    }
    ctx.stroke();
}
// Main Draw Function
function draw(){
    // clear canvas
    ctx.clearRect(0,0, WIDTH, HEIGHT);

    // draw pegs
    for (const peg of pegs) {
        if(peg.bonus){
            peg.drawPegBonus()
        } else {
            peg.drawPeg();
        }
    }

    for (const ball of balls) {
        // check if ball is saved -> collides with basket
        let caught = ball.checkCollisionBasket(basket);
        if (caught === 1) {
            temporaryControl = false;
            basket.moveVec = 1;
            balls.splice(balls.indexOf(ball),1);
            lives += 1;
            if (balls.length === 0) {
                addNewBall = true;
            }

        }

        // check if ball is colliding with sides
        let hitBottom = ball.checkCollisionSides(WIDTH, HEIGHT);
        if (hitBottom === 1) {
            temporaryControl = false;
            basket.moveVec = 1;
            balls.splice(balls.indexOf(ball),1);
            if (balls.length === 0) {
                addNewBall = true;
            }
        }

        // check if ball is colliding with pin
        let collidedPeg = ball.checkCollisionPegs(pegs);
        // if collided with pins add points
        if(collidedPeg){
            if(collidedPeg.bonus === true) {
                let randomizeBonus = Math.random();
                if(randomizeBonus < 0.2){
                    lives++;
                    addNewBall = true;
                }
                else if (randomizeBonus < 0.6) {
                    temporaryControl = true;
                }
                else if (basket.width < WIDTH/2){
                    basket.x -= 10;
                    basket.width = Math.min(basket.width + 20, WIDTH/2);
                } else {
                    ballPoints += 400;
                }
            }
            ballPoints += 100;
            multiplierBall = 1 + Math.floor(ballPoints/1000);
        }
    }
    let liveBalls = balls.filter(ball => ball.alive === 1).length;


    // move basket


    if (temporaryControl === true) {
        basket.moveVec = 0;
        if (moveRight === true && moveLeft === false) {
            basket.x = Math.min(basket.x + speed/framerate, WIDTH - basket.width);
        }
        if (moveLeft === true && moveRight === false) {
            basket.x = Math.max(basket.x - speed/framerate, 0);
        }
    } else {
        basket.checkCollisionSides(WIDTH);
    }
    basket.move(speed, framerate);
    basket.drawBasket();

    // move ball
    for (const ball of balls) {
        if(ball.alive === 1){
            ball.move(speed, framerate);
        }
    }
    if (balls.length === 0) {
        basket.x = basket.x + (basket.width - 100) / 2;
        basket.width = 100;
        score += ballPoints * multiplierBall;
        ballPoints = 0;
        multiplierBall = 1;
    }
    if (balls.length > liveBalls){
        drawLine();
    } else if (lives > 0 && addNewBall === true) {
        balls.push(new Ball(WIDTH/2, 12));
        addNewBall = false;
        drawLine();
    } else if (lives === 0 && liveBalls === 0){
        clearInterval(drawIntervall);
        again = confirm("Lost all lives! Scored " + score + " points! Retry?");
        if (again) {
            document.location.reload();
        }
    }
    for (const ball of balls) {
        ball.drawBall();
    }

    // draw lives & score
    drawLives();
    drawScore();
    if(balls.length > 0 && ballPoints > 0) {
        drawAddPoints()
    }

    // check if won
    if (pegs.length === 0){
        clearInterval(drawIntervall)
        again = confirm("You Won! Scored " + score + " points! Again?");
        if (again) {
            document.location.reload();
        }
    }
}

// mouse events
let mouseX;
let mouseY;
canvas.onmousemove = function (event) {
    let canvasBounds = canvas.getBoundingClientRect();
    mouseX = event.x - canvasBounds.x;
    mouseY = event.y - canvasBounds.y;
}
canvas.onclick = function () {
    // if ball available drop ball, remove 1 life
    let liveBalls = balls.filter(ball => ball.alive ===1).length;
    if(balls[liveBalls].alive === 0) {
        balls[liveBalls].vector[0] = mouseX - balls[liveBalls].x;
        balls[liveBalls].vector[1] = mouseY - balls[liveBalls].y;
        balls[liveBalls].normalizeVec();
        balls[liveBalls].alive = 1;
        lives--;
    }
    addNewBall = false;
}
// Keyboard events
document.addEventListener("keydown", keyDownHandler);
document.addEventListener("keyup", keyUpHandler);
function keyDownHandler (event) {
    if (event.key === "Rechts" || event.key === "ArrowRight" || event.key === "d") {
        moveRight = true;
    } else if (event.key === "Left" || event.key === "ArrowLeft" || event.key === "a") {
        moveLeft = true;
    }
}
function keyUpHandler (event) {
    if (event.key === "Right" || event.key === "ArrowRight" || event.key === "d") {
        moveRight = false;
    } else if (event.key === "Left" || event.key === "ArrowLeft" || event.key === "a") {
        moveLeft = false;
    }
}

// Running page
initializePegs();
document.body.onload = draw;
let drawIntervall = setInterval(draw,1000/framerate);
