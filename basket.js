export class Basket {
    x;
    y;
    width;
    height;
    moveVec;
    constructor(width, height, yBound) {
        this.x = 0;
        this.width = width;
        this.height = height;
        this.moveVec = 1;
        this.y = yBound - height;
    }
    move(speed, framerate) {
        this.x += this.moveVec * speed/framerate;
    }
    checkCollisionSides(xBound) {
        if (this.x < 0){
            this.moveVec = Math.abs(this.moveVec);
        }
        if (this.x > xBound - this.width) {
            this.moveVec = Math.abs(this.moveVec) * (-1);
        }
    }

}