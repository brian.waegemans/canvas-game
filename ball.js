export class Ball {
    radius;
    x;
    y;
    vector;
    alive;
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.radius = 8;
        this.alive = 0;
        this.vector = [0, 0];
    }
    // move in direction of the vector, add y velocity (falling)
    move(speed, framerate){
        this.x += this.vector[0] * speed/framerate;
        this.y += this.vector[1] * speed/framerate;
        this.vector[1] += 0.005 * speed/framerate;
    }
    // mirror vector if colliding with sides
    checkCollisionSides(xBound, yBound) {
        // left edge
        if(this.x < this.radius) {
            this.vector[0] = Math.abs(this.vector[0]);
        }
        // right edge
        if (this.x > xBound - this.radius){
            this.vector[0] = Math.abs(this.vector[0]) * (-1);
        }
        // top edge
        if (this.y < this.radius) {
            this.vector[1] = Math.abs(this.vector[1]);
        }
        // ball dies if touches bottom edge
        if(this.y > yBound - this.radius) {
            this.alive = 0;
            return 1;
        }
    }
    // normalize vector
    normalizeVec() {
        let vecLength = Math.sqrt(Math.pow(this.vector[0],2) + Math.pow(this.vector[1],2));
        this.vector[0] /= vecLength;
        this.vector[1] /= vecLength;

    }
    // check collision pegs
    checkCollisionPegs(pegs) {
        for (const peg of pegs) {
            let distance = peg.radius + this.radius;
            // is ball within square surrounding peg?
            if (Math.abs(peg.x - this.x) < distance
                && Math.abs(peg.y - this.y) < distance){
                // calculate exact cartesian distance
                if (Math.sqrt(Math.pow(this.x - peg.x, 2)
                    + Math.pow(this.y - peg.y, 2)) < distance) {
                    this.mirrorAlong(peg);
                    if(this.alive === 1){
                        pegs.splice(pegs.indexOf(peg),1);
                    }
                    // return if detected
                    return peg;

                }
            }
        }
    }
    // mirror along direct line between peg and ball centers
    mirrorAlong(peg) {
        let connectingVector = [this.x - peg.x, this.y - peg.y];
        let connLength = Math.sqrt(Math.pow(connectingVector[0],2)
            + Math.pow(connectingVector[1],2));
        let n = [connectingVector[0]/connLength, connectingVector[1]/connLength];

        let scalarProd = this.vector[0] * n[0] + this.vector[1] * n[1];
        if (scalarProd < 0) {
            this.vector[0] = this.vector[0] - 2 * scalarProd * n[0];
            this.vector[1] = this.vector[1] - 2 * scalarProd * n[1];
        }
    }
    // check collision with basket
    checkCollisionBasket(basket) {
        // is the ball low enough
        if (this.y + this.radius > basket.y) {
            // outside of width exact distance, otherwise safe
            if (this.x < basket.x) {
                if(Math.sqrt(Math.pow(this.x - basket.x,2)
                    + Math.pow(this.y - basket.y,2)) < this.radius) {
                    this.alive = 0;
                    return 1;

                }
            } else if (this.x > basket.x + basket.width) {
                if(Math.sqrt(Math.pow(this.x - basket.x,2)
                    + Math.pow(this.y - basket.y,2)) < this.radius) {
                    this.alive = 0;
                    return 1;
                }
            } else {
                this.alive = 0;
                return 1;
            }
        } else {
            return 0;
        }
    }
}